import './node_modules/swiper/dist/css/swiper.css';
import $ from 'jquery';
import Swiper from 'swiper';

const breakpoints3Cols = {
  1199: {
    slidesPerView: 2,
    spaceBetween: 40,
  },
  767: {
    slidesPerView: 1,
    spaceBetween: 30,
  },
  640: {
    slidesPerView: 1,
    spaceBetween: 20,
  },
  320: {
    slidesPerView: 1,
    spaceBetween: 10,
  },
};
const breakpoints2Cols = {
    1199: {
        slidesPerView: 2,
        spaceBetween: 30,
    },
    767: {
        slidesPerView: 1,
        spaceBetween: 30,
    },
    640: {
        slidesPerView: 1,
        spaceBetween: 20,
    },
    320: {
        slidesPerView: 1,
        spaceBetween: 10,
    },
};

$(() => {
  $('.swiper-container')
    .each(function () {
      const el = $(this)[0];
      const options = {
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },


      };

      if (el.classList.contains('swiper-3cols')) {
        options.slidesPerView = 3;
        options.spaceBetween = 30;
        options.breakpoints = breakpoints3Cols;
      }
      if (el.classList.contains('swiper-2cols')) {
        options.slidesPerView = 2;
        options.spaceBetween = 30;
        options.breakpoints = breakpoints2Cols;
      }
      if (el.classList.contains('swiper-1col')) {
          options.slidesPerView = 1;
      }

      if (el.classList.contains('no-pagination')) {
        delete options.pagination;
      }

      const mySwiper = new Swiper(el, options);
    });

  $('.tab-pane')
    .removeClass('active');
  $('.nav-with-slider .nav-link:first-of-type')
    .click();
});
