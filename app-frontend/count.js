import $ from 'jquery';
import './jquery.appear';
import CountUp from 'countup.js';

$(() => {
  $('.track')
    .appear();
  $('.track')
    .on('appear', (event, appearedElements) => {
      appearedElements.each(function () {
        const el = $(this);
        if (!el.hasClass('appeared')) {
          const countUp = new CountUp(el.attr('id'), 0, parseInt(el.data('count'), 10), 0, 2, {
            separator: ' ',
          });
          countUp.start();
        }
        el.addClass('appeared');
      });
    });

  $('.track-year')
    .appear();
  $('.track-year')
    .on('appear', (event, appearedElements) => {
      appearedElements.each(function () {
        const el = $(this);
        if (!el.hasClass('appeared')) {
          const countUp = new CountUp(el.attr('id'), 0, parseInt(el.data('count'), 10), 0, 2, {
            separator: '',
          });
          countUp.start();
        }
        el.addClass('appeared');
      });
    });
});


// $({ Counter: 0 }).animate({
//     Counter: $('.count-animate').text()
// }, {
//     duration: 1000,
//     easing: 'swing',
//     step: function() {
//       $('.count-animate').text(Math.ceil(this.Counter));
//     }
// });
