<?php

namespace App\Controller;

use App\Entity\AdviserNews;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdviserController extends AbstractController
{
    /**
     * @Route("/poradce", name="adviser")
     */
    public function index()
    {

        $news = $this->getDoctrine()->getRepository(\App\Entity\AdviserNews::class)->getUnreadNews($this->getUser());

        $calculators = $this->getDoctrine()->getRepository(\App\Entity\Calculator::class)->findBy([], ['id' => 'DESC']);

        return $this->render('adviser/index.html.twig', [
            'controller_name' => 'AdviserController',
            'news' => $news,
            'calculators' => $calculators
        ]);
    }

    /**
     * @Route("poradce/kalkulacky", name="calculators")
     */
    public function calculators()
    {
        $calculators = $this->getDoctrine()->getRepository(\App\Entity\Calculator::class)->findBy([], ['id' => 'DESC']);

        return $this->render('adviser/calculators.html.twig', [
            'controller_name' => 'AdviserController',
            'calculators' => $calculators
        ]);
    }

    /**
     * @Route("poradce/novinky/{category}", name="news", defaults={"category"=0})
     */
    public function news($category)
    {

        $repository = $this->getDoctrine()->getRepository(\App\Entity\AdviserNews::class);

        if ($category) {
            $news = $repository->findBy(['category' => $category], ['id' => 'DESC']);
        }else{
            $news = $repository->findBy([], ['id' => 'DESC']);
        }


        return $this->render('adviser/news.html.twig', [
            'controller_name' => 'AdviserController',
            'news' => $news,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("poradce/novinka/{id}", name="news_detail")
     */
    public function newsdetail($id){
        $news = $this->getDoctrine()->getRepository(\App\Entity\AdviserNews::class)->find($id);

        return $this->render('adviser/detail.html.twig', [
            'controller_name' => 'AdviserController',
            'novinka' => $news,
            'user' => $this->getUser()
        ]);
    }


    /**
     * @Route("poradce/novinky/precteno/{id}", name="news_read")
     */
    public function markasread($id){
        /**@var AdviserNews $novinka*/
        $novinka = $this->getDoctrine()->getRepository(\App\Entity\AdviserNews::class)->find($id);

        if ($novinka) {
            $user =  $this->getUser();
            $novinka->addReader($user);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('news_detail', ['id' => $id]);
    }


    /**
     * @Route("poradce/novinky/neprecteno/{id}", name="news_unread")
     */
    public function markasunread($id){
        /**@var AdviserNews $novinka*/
        $novinka = $this->getDoctrine()->getRepository(\App\Entity\AdviserNews::class)->find($id);

        if ($novinka) {
            $user =  $this->getUser();
            $novinka->removeReader($user);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('news_detail', ['id' => $id]);
    }
}
