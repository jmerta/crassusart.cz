<?php

namespace App\Controller;

use App\dataObj\staticData\Assistance;
use App\dataObj\staticData\Sections;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InsuranceController extends AbstractController
{
    /**
     * @Route("/pojisteni")
     */
    public function index()
    {
        return $this->render('insurance/index.html.twig', [
            'controller_name' => 'InsuranceController',
            'insuranceSections' => Sections::getInsuranceData(),
            'assistance' => Assistance::getData(),
        ]);
    }
}
