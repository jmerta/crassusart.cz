<?php

namespace App\Controller;

use App\dataObj\staticData\Bosses;
use App\dataObj\staticData\References;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends AbstractController
{
    /**
     * @Route("o-nas")
     */
    public function index()
    {
        return $this->render('about/index.html.twig', [
            'controller_name' => 'AboutController',
            'references' => References::getData(),
            'bosses' => Bosses::getData(),
        ]);
    }
}
