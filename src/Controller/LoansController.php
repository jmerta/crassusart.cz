<?php

namespace App\Controller;

use App\dataObj\staticData\Sections;
use App\dataObj\staticData\SectionTiles;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LoansController extends AbstractController
{
    /**
     * @Route("/uvery")
     */
    public function index()
    {
        return $this->render('loans/index.html.twig', [
            'controller_name' => 'LoansController',
            'loansSections' => Sections::getLoansData(),

        ]);
    }
}
