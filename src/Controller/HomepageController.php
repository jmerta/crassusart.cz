<?php

namespace App\Controller;

use App\dataObj\staticData\Event;
use App\dataObj\staticData\Tiles;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        $clientsNumber = 4257;
        $partnersNumber = 40;
        $yearsNumber = date('Y') - 2009;

        $event = $this->getDoctrine()->getRepository(\App\Entity\Event::class)->findEventToHP();


        return $this->render('homepage/index.html.twig', [
            'controller_name' => 'HomepageController',
            'clientsNumber' => $clientsNumber,
            'partnersNumber' => $partnersNumber,
            'yearsNumber' => $yearsNumber,
            'tileInfo1' => Tiles::getData()[0],
            'tileInfo2' => Tiles::getData()[1],
            'tileInfo3' => Tiles::getData()[2],
            'partners' => [
                ['logo' => 'Crassusart_partneri__01_kooperativa.svg', 'link' => 'https://www.koop.cz'],
                ['logo' => 'Crassusart_partneri__02_uniqa.svg', 'link' => 'https://www.uniqa.cz'],
                ['logo' => 'Crassusart_partneri__03_cpp.svg', 'link' => 'https://www.cpp.cz'],
                ['logo' => 'Crassusart_partneri__05_kb_pojistovna.svg', 'link' => 'https://www.kb.cz'],
                ['logo' => 'logo_Generali_CP_BOX_2019_sekundarni_CMYK_Linky.png', 'link' => 'https://www.generali.cz'],
                ['logo' => 'Crassusart_partneri__08_csob.svg', 'link' => 'https://www.csob.cz/portal/'],
                ['logo' => 'Crassusart_partneri__09_equbank.svg', 'link' => 'https://www.equabank.cz/'],
                ['logo' => 'Crassusart_partneri__10_raiffeisen.svg', 'link' => 'https://www.rb.cz'],
                ['logo' => 'Crassusart_partneri__11_raiffeisen_sporitelna.svg', 'link' => 'https://www.rsts.cz/'],
                ['logo' => 'Crassusart_partneri__12_unicredit.svg', 'link' => 'https://www.unicreditbank.cz'],
                ['logo' => 'Crassusart_partneri__13_ceska_sporitelna.svg', 'link' => 'https://www.csas.cz'],
                ['logo' => 'Crassusart_partneri__14_hypotecni_banka.svg', 'link' => 'https://www.hypotecnibanka.cz'],
                ['logo' => 'Crassusart_partneri__15_moneta.svg', 'link' => 'https://www.moneta.cz'],
                ['logo' => 'Crassusart_partneri__16_wustenrot.svg', 'link' => 'https://www.wuestenrot.cz/'],
//                ['logo' => 'Crassusart_partneri__17_amundi.svg', 'link' => 'https://www.amundi.cz'],
//                ['logo' => 'Crassusart_partneri__19_tesla.svg', 'link' => 'https://www.teslainvest.cz'],
//                ['logo' => 'Crassusart_partneri__20_conseq.svg', 'link' => 'https://www.conseq.cz'],
//                ['logo' => 'Crassusart_partneri__21_eic.svg', 'link' => 'https://eic.eu/'],
//                ['logo' => 'Crassusart_partneri__22_moventum.svg', 'link' => 'https://www.moventum.cz/']
            ],
            'event' => $event ? Event::fromEvent($event) : null,
        ]);
    }
}
