<?php

namespace App\dataObj\staticData;


use App\dataObj\declarations\BossDataObj;

class Bosses
{
    public static function getData()
    {
        return array(
            new BossDataObj(
                "Bc. Dušan Zykl, MBA",
                "Jednatel společnosti",
                "dusan_zykl.jpg",
                "<p>Dušan Zykl působí od roku 1997 v pojišťovací a makléřské oblasti, kde zpočátku zastával pozice finančního poradce, manažera a školitele. V roce 2000 se stal oblastním ředitelem Komerční pojišťovny a.s., ve které se podílel na plánování a realizaci obchodní činnosti v jemu svěřeném regionu. Organizoval jedinečný přestup obchodní služby z Komerční pojišťovny do Generali.

Na pozici oblastního ředitele začal pracovat roku 2003 i v Generali pojišťovna a.s., kde mimo jiné určoval i základní strategii ředitelství, plánování a realizaci regionálního marketingu. Nové oblastní ředitelství vybudoval na „zelené louce“. Od září 2009 je jednatelem společnosti CrassusArt, s.r.o.

Roku 2000 ukončil studium na fakultě ekonomicko-správní v Pardubicích s bakalářským titulem a v roce 2009 úspěšně absolvoval také MBA – Senior Executivem, B.I.B.S., a.s., The Nottingham Trent University.

Ve volném čase se věnuje především své rodině a ze sportovních aktivit vyhledává nejraději badminton a tenis. </p>"
            ),
            new BossDataObj(
                "Iveta Bryxová",
                "TOP management",
                "iveta_bryxova.jpg",
                "Roku 2005 začala pracovat v Generali pojišťovně na pozici referentky obchodu. Od roku 2008 pak na pozici asistentky ředitelky ve Službách města Pardubic.

Po mateřské dovolené vstoupila roku 2016 do makléřské společnosti Exkaso na pozici referentky.

Po půlroce změnila zaměstnavatele a od ledna 2016 působí v CrassusArt na pozici manažerky, kde úspěšně buduje svůj obchodní tým.

Zároveň pracuje ve firmě jako IT podpora.

Ve svém volném čase ráda cvičí, hraje badminton a především se věnuje své rodině. "
            ),
            new BossDataObj(
                "Eva Medunová",
                "TOP management",
                "eva_medunova.jpg",
                "<p>Eva Medunová začala podnikat hned po roce 1990, především v obchodě. Jako první v ČR začala prodávat vlajkové stožáry.</p>
<p>Kariéru ve finančnictví začala v roce 2000 v Komerční pojišťovně, kde pracovala na pozici poradce. Byla součástí týmu, který budoval zcela nové oblastní ředitelství.</p>
<p>V roce 2003 zrušil nový vlastník Komerční banky Societe General obchodní službu. S týmem kolegů přešla na jaře 2003 pracovat do Generali pojišťovny a.s. Po velmi rychlém kariérním postupu dosáhla nejvyššího stupně kariéry - generálního reprezentanta.
Po více jak 10 letech, v roce 2014, odešla z Generali pojišťovny a začala pracovat v makléřské společnosti CrassusArt, s.r.o., kde viděla větší portfoliové možnosti pro sebe a své další klienty. Od 1. 11.2014 pracuje jako poradce a zároveň je součástí TOP managementu firmy.</p>
<p>Ve svém volném čase ráda jezdí na kole, chodí do divadla a volného času si užívá se svojí rodinou při dobrém víně.</p>"
            ),
            new BossDataObj(
                'David Žáček',
                'Hlavní analytik',
                'david_zacek.jpg',
                ''
            )
        );

    }
}
