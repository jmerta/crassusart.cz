<?php

namespace App\dataObj\staticData;


use App\dataObj\declarations\components\TileDataObj;

class Tiles
{
    public static function getData()
    {
        return array(
            new TileDataObj(
                "Pojištění",
                "Finanční zajištění
                vlastní osoby, dětí a partnera v případě úmrtí, úrazu nebo nemoci",
                "Pojisteni.svg",
                "pojisteni"
            ),
            new TileDataObj(
                "Investice",
                "Vytváříme investiční řešení šité na míru dle požadovaných cílů klienta",
                "Investice.svg",
                "investice"
            ),
            new TileDataObj(
                "Úvěry",
                "Zajišťujeme zprostředkování spotřebitelských, hypotečních úvěrů aj.",
                "Uvery.svg",
                "uvery"
            )
        );
    }
}
