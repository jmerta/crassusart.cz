<?php

namespace App\dataObj\staticData;




use App\dataObj\declarations\AssistanceDataObj;

class Assistance
{
    public static function getData()
    {
        return array (
            new AssistanceDataObj(
                "Allianz",
              "Crassusart_likvidace__allianz.svg",
              "https://www.allianz.cz/pro-klienty/oznamit-novou-udalost/",
              [
                  "241 170 000"
              ]
            ),
            new AssistanceDataObj(
                "ČSOB",
                "Crassusart_likvidace__csob.svg",
                " https://www.csobpoj.cz/skody-a-pojistne-udalosti",
                [
                    "800 100 777",
                    "222 803 442 – asistence"
                ]
            ),
            new AssistanceDataObj(
                "Generali Česká poj.",
                "logo_Generali_CP_BOX_2019_sekundarni_CMYK_Linky.png",
                "https://www.ceskapojistovna.cz/skody-a-pojistne-udalosti/nahlasit",
                [
                    "841 114 114"
                ]
            ),
            new AssistanceDataObj(
                "Kooperativa",
                "Crassusart_likvidace__kooperativa.svg",
                "https://insure.koop.cz/hlasenky-web/faces/vstup_info.xhtml",
                [
                    "841 105 105"
                ]
            ),
            new AssistanceDataObj(
                "VZP",
                "Crassusart_likvidace__vzp.svg",
                "https://www.vzp.cz",
                [
                    "952 222 222"
                ]
            ), new AssistanceDataObj(
                "Uniqua",
                "Crassusart_likvidace__uniqa.svg",
                "https://www.uniqa.cz/hlaseni-skod/",
                [
                    "+420 488 125 125 "
                ]
            ),new AssistanceDataObj(
                "ČPP",
                "Crassusart_likvidace__cpp.svg",
                " https://www.cpp.cz/nahlasit-skodu/",
                [
                    "+420 957 444 555"
                ]
            ),
        );
    }
}
