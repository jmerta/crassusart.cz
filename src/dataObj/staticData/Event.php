<?php

namespace App\dataObj\staticData;


use App\dataObj\declarations\EventDataObj;

class Event
{
    public static function getData()
    {
        return (
        new EventDataObj(
            "Reprezentační ples",
            "<p>Dne 1. 1. 2019
                        pořádáme reprezentační
                        firemní ples CrassusArt</p>
                    <p>V Zleté štice. Moderuje
                        Martin Vencl.</p>
                    <p>Vstupenky je možné
                        zakoupit na pobočkách
                        CrassusArt.</p>"
        )
        );
    }

    public static function fromEvent(\App\Entity\Event $event)
    {
        return
        new EventDataObj(
            $event->getHeadline(),
            $event->getText()
        );
    }
}
