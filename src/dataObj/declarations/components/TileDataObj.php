<?php

namespace App\dataObj\declarations\components;



class TileDataObj
{

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $subTitle;

    /**
     * @var string
     */
    public $icon;

    /**
     * @var string
     */
    public $link;

    /**
     * TileDataObj constructor.
     * @param string $title
     * @param string $subTitle
     * @param string $icon
     * @param string $link
     */
    public function __construct(string $title, string $subTitle, string $icon, string $link)
    {
        $this->title = $title;
        $this->subTitle = $subTitle;
        $this->icon = $icon;
        $this->link = $link;
    }


}
