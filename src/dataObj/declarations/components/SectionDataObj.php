<?php

namespace App\dataObj\declarations\components;



class SectionDataObj
{

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $subTitle;

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $iconTile;

    /**
     * @var string
     */
    public $iconParagraph;

    /**
     * @var string[]
     */
    public $text;

    /**
     * SectionDataObj constructor.
     * @param string $title
     * @param string $subTitle
     * @param string $id
     * @param string $iconTile
     * @param string $iconParagraph
     * @param string[] $text
     */
    public function __construct(string $title, string $subTitle, string $id, string $iconTile, string $iconParagraph, array $text)
    {
        $this->title = $title;
        $this->subTitle = $subTitle;
        $this->id = $id;
        $this->iconTile = $iconTile;
        $this->iconParagraph = $iconParagraph;
        $this->text = $text;
    }

}
