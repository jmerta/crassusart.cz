<?php

namespace App\dataObj\declarations;



class ReferenceDataObj
{

    /**
     * @var string
     */
    public $img;

    /**
     * @var string
     */
    public $link;

    /**
     * @var array
     */
    public $imgClasses;

    /**
     * TileDataObj constructor.
     * @param string $img
     * @param string $link
     */
    public function __construct($img, $link, $imgClasses = ['w-100'])
    {
        $this->img = $img;
        $this->link = $link;
        $this->imgClasses = $imgClasses;
    }


}
