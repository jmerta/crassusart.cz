<?php

namespace App\dataObj\declarations;



class PositionsDataObj
{

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var PositionSectionDataObj[]
     */
    public $sections;

    /**
     * PositionsDataObj constructor.
     * @param string $id
     * @param string $name
     * @param PositionSectionDataObj[] $sections
     */
    public function __construct($id, $name, array $sections)
    {
        $this->id = $id;
        $this->name = $name;
        $this->sections = $sections;
    }


}
