<?php

namespace App\dataObj\declarations;



class PositionSectionDataObj
{

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $icon;

    /**
     * @var string[]
     */
    public $items;

    /**
     * SectionTextDataObj constructor.
     * @param string $title
     * @param string $icon
     * @param string[] $items
     */
    public function __construct($title, $icon, array $items)
    {
        $this->title = $title;
        $this->icon = $icon;
        $this->items = $items;
    }


}
